class ScolaError extends Error {
  constructor(message, code, category, origin, detail) {
    super(message);

    this.code = code;
    this.category = category;
    this.origin = origin;
    this.detail = detail;
  }
}

module.exports = ScolaError;
